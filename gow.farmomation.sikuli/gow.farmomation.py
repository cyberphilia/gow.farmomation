

from city import City,Ingestor

image_path = 'images/andy/1920x1080/'


# myApp = App("Andy")
# if not myApp.window(): # no window(0) - Andy not open
#         App.open('"C:\\Program Files\\Andy\\HandyAndy.exe" startandy')
#         wait(30)


# myApp.focus()
# wait(1)


sleep(5)

App.focusedWindow().highlight(2)

reg = Region(App.focusedWindow())
# reg = Region(6,32,605,999)  #Sikuli will only search for images in this region 
                            # of the screen
reg.setThrowException(False) # will not throw an exception if image not found




def launchGOW():
    print('launchGOW')
    reg.click(str(image_path + 'gow_icon.png'))    


def login(email,password):
    print('login')
    reg.wait(str(image_path + 'login_prompt.png'),30) # wait upto 30 seconds for the login prompt
    if(reg.exists(str(image_path + 'login_email.png'))):
        reg.click(str(image_path + 'login_email.png'))
        sleep(1)
        reg.type(email)
        reg.click(str(image_path + 'login_password.png'))
        sleep(1)
        reg.type(password)
        reg.click(str(image_path + 'login_button.png'))

    if(reg.exists(str(image_path + 'login_failure.png'))):
        reg.click(str(image_path + 'login_failure_ok.png'))
        return False        
    else:           
        reg.wait(str(image_path + 'login_buy.png'),60)
        reg.type(Key.ESC)
        return True

def logout():
    print('logout')
    reg.click(str(image_path + 'bottom_menu_more.png'))
    reg.wait(str(image_path + 'mz_button.png'),60)
    reg.click(str(image_path + 'mz_button.png'))
    reg.click(str(image_path + 'logout_button.png'))
    reg.click(str(image_path + 'logout_yes.png'))

def wait_for_chat():
    print('wait_for_chat')
    reg.waitVanish(str(image_path + 'connecting_to_chat.png'),120)

def helps():
    print('helps')
    if(reg.exists(str(image_path + 'help_hands.png'))):         
        reg.click(str(image_path + 'help_hands.png'))
        reg.wait(str(image_path + 'help_helpall.png'))
        reg.click(str(image_path + 'help_helpall.png'))
        reg.click(str(image_path + 'sub_screen_back.png'))
     

def open_alliance_gifts():
    print('open_alliance_gifts')
    if(reg.exists(Pattern(str(image_path + 'bottom_menu_alliance_with_gift.png')).similar(0.82))):
        reg.click(Pattern(str(image_path + 'bottom_menu_alliance_with_gift.png')).similar(0.82))
        reg.wait(str(image_path + 'alliance_gift.png'),10)
        reg.click(str(image_path + 'alliance_gift.png'))
        while(reg.exists(str(image_path + 'alliance_gift_open.png'))):
            reg.click(str(image_path + 'alliance_gift_open.png'))
            reg.click(str(image_path + 'alliance_gift_clear.png'))
        back_to_main_screen()
        open_alliance_gifts()
    

def back_to_main_screen():
    while(reg.exists(str(image_path + 'sub_screen_back.png'))):
        reg.click(str(image_path + 'sub_screen_back.png'))
        sleep(1)
def open_secert_chest():
    print('open_secert_chest')
    reg.click(Pattern(str(image_path + 'secert_gift_chest.png')).targetOffset(-1,9))
    reg.click(str(image_path + 'gift_collect.png'))
    reg.click(str(image_path + 'gift_ok.png'))


def open_athenas_chest():
    print('open_athenas_chest')
    reg.click(Pattern(str(image_path + 'athena_gift_chest.png')).targetOffset(-1,9))
    reg.click(str(image_path + 'gift_collect.png'))
    reg.click(str(image_path + 'gift_ok.png'))


def open_treasures():
    print('open_treasures')
    reg.click(str(image_path + 'bottom_meun_items.png'))
    reg.wait(str(image_path + 'my_items.png'),60)
    reg.click(Pattern(image_path + 'my_items.png').similar(0.82))
    reg.click(str(image_path + 'treasures.png'))
    while(reg.exists(str(image_path + 'use.png'))):
        reg.click(str(image_path + 'use.png'))
        sleep(1)
        reg.type(Key.ESC)
        sleep(1)
  
def city_quests(quest_type_img_name):
    print('city_quests')
    reg.click(str(image_path + 'bottom_meun_quests.png'))
    sleep(5)

    reg.click(str(image_path + quest_type_img_name))
    while(reg.exists(str(image_path + 'quests_auto_complete.png'))):
        reg.click(str(image_path + 'quests_auto_complete.png'))
        sleep(1)
    while(reg.exists(str(image_path + 'quests_collect.png'))):
        reg.click(str(image_path + 'quests_collect.png'))
        sleep(1)

    reg.click(str(image_path + 'quests_start.png'))

    back_to_main_screen()



ingestor = Ingestor()
cities = ingestor.get_cites('cities.csv')
for city in cities:
    launchGOW()
    if(login(city.email,city.password)):
        # sleep(120)
        sleep(10)
        wait_for_chat()
        sleep(5)
        sleep(5)
        helps()
        sleep(5)
        open_alliance_gifts()
        sleep(5)
        open_secert_chest()
        sleep(5)
        open_athenas_chest()
        sleep(5)
        open_treasures()
        if(city.city_quests_empire == 'True'):
            city_quests('quests_empire.png')
            sleep(5)
        if(city.city_quests_daily == 'True'):
            city_quests('quests_daily.png')
            sleep(5)
        if(city.city_quests_alliance == 'True'):
            city_quests('quests_alliance.png')
            sleep(5)
        if(city.city_quests_vip == 'True'):
            city_quests('quests_vip.png')
        sleep(5)
        logout()
        sleep(15)

print('done..')