import csv
import os
import inspect

class City(object):
    city_quests_empire = None
    city_quests_daily = None
    city_quests_alliance = None
    city_quests_vip = None

    def __init__(self, email,password):
        self.email = email
        self.password = password

class Ingestor(object):
    # @staticmethod
    def get_cites(self,cities_file):
        cities = []
        rows = self.get_cities_rows(cities_file)
        # print (rows)
        for row in rows:
            city = City(row[0],row[1])
            city.city_quests_empire = row[2]
            city.city_quests_daily = row[3]
            city.city_quests_alliance = row[4]
            city.city_quests_vip = row[5]
            cities.append(city)
        return cities

    def get_cities_rows(self,cities_file):
        rows = []
        csv_reader = csv.reader(open(os.path.abspath(cities_file)) \
            ,delimiter=',', quotechar='|')
        csv_reader.next()  # skip the headers
        for row in csv_reader:
            rows.append(row)

        return rows


