# Game of War Automation

These scripts will allow you to automate tasks in Game of War.

## Dependencies
* Virtual Box
* Andy
* Sikuli

## Assumptions

You will be running the sikuli script from the commandline.  you will call the cscript from the root of the git repo.  in the root of the repo you will have a cities.csv file.

## Setup

You will need to create csv file with the following columns.

* email
* password
* city_quests_empire (boolean)
* city_quests_daily (boolean)
* city_quests_alliance (boolean)
* city_quests_vip (boolean)

## Farm Automation Features

This script will loop through your farms and complete configured tasks.

#### Available Tasks

* launch game of war
* login
* help all
* open alliance gifts
* open secert chest
* open athenas chest
* open treasures
* logout

#### On horizon

* player donate
* alliance city donate
* train troops
* train traps
* renew shield
* send message
* quests
* alliance quest


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact